const {resolve} = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
const extractTextPlugin = require('extract-text-webpack-plugin');
const duplicateCheckerPlugin = require('duplicate-package-checker-webpack-plugin');
const compressionPlugin = require('compression-webpack-plugin');

const packagedef = require('./package.json');

module.exports = env => {
  const production = env && env.production;

  const extractSass = new extractTextPlugin({
    filename: '[name].css',
    disable: !production
  });

  return {
    entry: {
      'app': './index.ts'
    },
    output: {
      path: resolve(__dirname, 'dist'),
      filename: "[name].js"
    },
    module: {
      rules: [
        {
          test: /\.s[c|a]ss$/,
          loader: extractSass.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  minimize: production
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: () => [
                    require('autoprefixer')
                  ]
                }
              },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: true,
                  includePaths: []
                }
              }
            ],
            fallback: 'style-loader'
          })
        },
        {
          test: /\.ts$/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                silent: env && env.tssilent
              }
            },
            {
              loader: 'tslint-loader',
              options: {
                configFile: production ? 'tslint.config.js' : 'tslint.config.dev.js'
              }
            }
          ]
        },
        {
          test: /\.(eot|ttf|woff|woff2|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'assets/fonts/'
              }
            }
          ]
        }, {
          test: /\.(png|jpeg|jpg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'assets/images/'
              }
            }
          ]
        }, {
          test: /\.html$/,
          loader: 'html-loader'
        }
      ]
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        names: ['common']
      }),
      new duplicateCheckerPlugin(),
      production ? new compressionPlugin() : null,
      production ? null : new webpack.NamedModulesPlugin(),
      new htmlWebpackPlugin({
        title: packagedef.name
      }),
      extractSass
    ].filter(Boolean),
    resolve: {
      alias: {
        'juice': resolve(__dirname, 'node_modules/node-juice-sass/_juice.scss'),
        'font-awesome': resolve(__dirname, 'node_modules/font-awesome/scss/font-awesome.scss')
      },
      extensions: [".js", ".ts", ".json"]
    },
    devtool: production ? false : 'cheap-module-eval-source-map'
  };
};